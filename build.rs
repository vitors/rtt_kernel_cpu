use std::fs;
use std::path::PathBuf;
use std::env;

fn main() {
    let out_dir = PathBuf::from(env::var("OUT_DIR").unwrap());
    println!("cargo:rustc-link-search={}", out_dir.display());
    let libcontext = out_dir.clone().join("libcontext.a");
    let memory = out_dir.clone().join("memory.x");
    fs::copy("./libcontext.a", libcontext).unwrap();
    fs::copy("./memory.x", memory).unwrap();
}